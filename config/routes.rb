Rails.application.routes.draw do

  namespace :v1 do
    resources :wishlists
    resources :venues
    resources :users
    scope :foursquare do
      post 'auth', to: 'foursquare#auth'
    end
  end

end
