module JwtHandler
  extend ActiveSupport::Concern

  included do
    before_action :check_auth
  end

  def check_auth
    data = { error: 'Authorization error.' }

    unless request.headers['Authorization'].nil?
      token = request.headers['Authorization'].split
      unless token[1].nil?
        data = jwt_decode(token[1])
        if data.nil?
          render json: data, status: 401
          return;
        end
      end
    else
      render json: data, status: 401
      return;
    end
  end

  def jwt_encode(payload)
    JWT.encode payload, Rails.application.secrets.jwt_secret, 'HS256' rescue nil
  end

  def jwt_decode(token)
    JWT.decode token, Rails.application.secrets.jwt_secret, true, { :algorithm => 'HS256' } rescue nil
  end

end
