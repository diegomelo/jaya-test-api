module HasScopeHandler
  extend ActiveSupport::Concern

  included do
    has_scope :by_degree
    has_scope :by_fsq_user
    has_scope :by_fsq_venue
    has_scope :by_user
    has_scope :by_venue
  end
end