module FoursquareHandler
  require 'uri'
  require 'net/http'
  require 'json'

  FOURSQUARE_API_CONFS = {
    redirect_url: 'http://localhost:4200/foursquare/callback',
    client_id: 'TFUOZTMDNWTSMK2H0SJE4KFTWTIMDZ44GVWP0RMP4WZZHE11',
    client_secret: 'CEBGMTKLO11Y4UJWN2QHWTNAQIDVXE4HSND25GHKCSHCABDR',
    version: '20170813'
  }

  def get_access_token(code)
      url_string = "https://foursquare.com/oauth2/access_token?client_id=#{FOURSQUARE_API_CONFS[:client_id]}&client_secret=#{FOURSQUARE_API_CONFS[:client_secret]}&grant_type=authorization_code&redirect_uri=#{FOURSQUARE_API_CONFS[:redirect_url]}&code=#{code}"

      url = URI(url_string)

      http = Net::HTTP.new(url.host, url.port)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE

      request = Net::HTTP::Get.new(url)

      response = http.request(request)
      JSON.parse(response.read_body) rescue { error: 'Error on fetch access token.' }
  end

  def get_logged_user_data(token)
      url_string = "https://api.foursquare.com/v2/users/self?oauth_token=#{token}&v=#{FOURSQUARE_API_CONFS[:version]}"

      url = URI(url_string)

      http = Net::HTTP.new(url.host, url.port)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE

      request = Net::HTTP::Get.new(url)

      response = http.request(request)
      JSON.parse(response.read_body) rescue { error: 'Error on fetch data.' }
  end

end
