class ApplicationController < ActionController::API
  include JwtHandler
  include HasScopeHandler
end
