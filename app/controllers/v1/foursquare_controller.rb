module V1
  class FoursquareController < ApplicationController
    skip_before_action :check_auth, only: [:auth]

    include FoursquareHandler

    def auth
      data = { error: 'Missing code param.' }
      if params[:code]
        access_token_response = get_access_token(params[:code])
        unless access_token_response['access_token'].nil?
          logged_user_data = get_logged_user_data(access_token_response['access_token'])
          save_user_id_if_not_exists(logged_user_data)
          data = {
            jwt: set_user_token(logged_user_data),
            access_token: access_token_response['access_token']
          }
        else
          data = access_token_response
        end
      end

      render json: data
    end

    private

    def save_user_id_if_not_exists(data)
      unless data['response']['user'].nil?
        User.find_or_create_by(fsq_user_id: data['response']['user']['id']) unless data['response']['user']['id'].nil?
      end
    end

    def set_user_token(data)
      unless data['response']['user'].nil?
        user_data = {
          user_id: data['response']['user']['id'],
          first_name: data['response']['user']['firstName'],
          last_name: data['response']['user']['lastName'],
          photo: data['response']['user']['photo'],
        }
        jwt_encode(user_data)
      end
    end
  end
end
