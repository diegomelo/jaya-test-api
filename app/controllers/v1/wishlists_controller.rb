module V1
  class WishlistsController < ApplicationController
    before_action :set_wishlist, only: [:show, :update, :destroy]

    # GET /wishlists
    def index
      @wishlists = apply_scopes(Wishlist).all

      render json: @wishlists, include: [:user, :venue]
    end

    # GET /wishlists/1
    def show
      render json: @wishlist
    end

    # POST /wishlists
    def create
      create_params = wishlist_params_interceptor
      @wishlist = Wishlist.new(create_params)

      if @wishlist.save
        render json: @wishlist, status: :created, location: [:v1, @wishlist]
      else
        render json: @wishlist.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /wishlists/1
    def update
      if @wishlist.update(wishlist_params)
        render json: @wishlist
      else
        render json: @wishlist.errors, status: :unprocessable_entity
      end
    end

    # DELETE /wishlists/1
    def destroy
      @wishlist.destroy
    end

    private
      def wishlist_params_interceptor
        create_params = wishlist_params
        user = User.find_or_create_by(fsq_user_id: create_params[:user_id])
        venue = Venue.find_or_create_by(fsq_venue_id: create_params[:venue_id])
        create_params[:user_id] = user.id
        create_params[:venue_id] = venue.id
        return create_params
      end
      # Use callbacks to share common setup or constraints between actions.
      def set_wishlist
        @wishlist = Wishlist.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def wishlist_params
        params.require(:wishlist).permit(:user_id, :venue_id)
      end
  end
end
