class User < ApplicationRecord
  has_many :venues

  scope :by_fsq_user, -> fsq_user_id { where(:fsq_user_id => fsq_user_id) }

  validates :fsq_user_id, presence: true
end
