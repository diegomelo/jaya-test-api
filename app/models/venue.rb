class Venue < ApplicationRecord
  has_one :user

  scope :by_fsq_venue, -> fsq_venue_id { where(:fsq_venue_id => fsq_venue_id) }

  validates :fsq_venue_id, presence: true
end
