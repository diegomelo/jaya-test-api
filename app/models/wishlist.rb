class Wishlist < ApplicationRecord
  belongs_to :user
  belongs_to :venue

  scope :by_user, -> user_id { where(user_id: User.find_or_create_by(fsq_user_id: user_id).id) }
  scope :by_venue, -> venue_id { where(venue_id: Venue.find_or_create_by(fsq_venue_id: venue_id).id) }

  validates :user_id, presence: true
  validates :venue_id,
    presence: true,
    uniqueness: {
      scope: :user_id
    }
end
